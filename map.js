const map = L.map('mapid').setView([43.505, 12.09],7);

// L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
//     attribution: 'Map data © <a href="https://openstreetmap.org">OpenStreetMap</a> contributors',
//     maxZoom: 18
// }).addTo(mymap);

L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox.streets',
    accessToken: 'pk.eyJ1IjoibHVjYW1hcmlhbmkiLCJhIjoiY2pzaTQ1ZTUzMjI5ajQ5bzZ3ZDJ6Z2VrYyJ9.SSn5d7sECRSvoRWyJLl4qw'
}).addTo(map);


const marker = L.marker([42.5, 12.09]).addTo(map);
marker.bindPopup("<b>Hello world!</b><br>I am a popup.").openPopup();
